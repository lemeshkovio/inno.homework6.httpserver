package inno.lemeshkov;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

public class MyHttpHandler implements HttpHandler {
    private String response;

    /**Обрабатывает любой Get запрос возвращая на него
     * список файлов и папок в текущей директории
     *
     * @param exchange - запрос пользователя
     * @throws IOException - при попытке сформировать ответ на запрос возмощен IOException
     */
    @Override
    public void handle(HttpExchange exchange) throws IOException {

        if(exchange.getRequestMethod().equals("GET")) {
            File dir = new File(System.getProperty("user.dir"));
            File[] files = dir.listFiles();
            if(files!=null) {
                response = fileToString(files);
                handleResponse(exchange, fileToString(files));
            }
            System.out.println(exchange.getRequestMethod());
        }
        else{
            exchange.sendResponseHeaders(404,-1);
        }
        System.out.println(exchange.getRequestMethod());
    }

    private String fileToString(File[] files){
        response = "";
        for(File file : files){
            response = response + file.getName();
            response = response +"\n";
        }
        return response;
    }

    private void handleResponse(HttpExchange exchange, String response) throws IOException {
        try(OutputStream outputStream = exchange.getResponseBody()){
            exchange.sendResponseHeaders(200,response.length());
            outputStream.write(response.getBytes());
            outputStream.flush();
        }catch(Throwable e){
            System.err.println("Can't send response\n" + e);
        }
    }
}
